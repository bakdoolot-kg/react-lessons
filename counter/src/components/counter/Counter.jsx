import React, {Component} from 'react';
import './counter.scss'

class Counter extends Component {
    state = {
        count: 0
    };

    counterInc = () => {
        this.setState(
            (oldCount) => ({count: oldCount.count + 1 })
        )
    }
    counterDec = () => {
        this.setState(
            ({count}) => ({count: count - 1})
        )
    }
    counterReset = () => {
        this.setState(
            ({count}) => ({count: 0})
        )
    }

    render() {
        return (
            <div className='counter'>
                <p className='count'>{this.state.count}</p>
                <button className='dec' onClick={this.counterDec}>Decrement</button>
                <button className='inc' onClick={this.counterInc}>Increment</button>
                <button className='reset' onClick={this.counterReset}>Reset</button>

            </div>
        );
    }
}

export default Counter;