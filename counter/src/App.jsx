import "./app.css";
import { Header, Counter} from "./components";

function App() {
  return (
    <div className="app">
      <Header />
        <Counter/>
    </div>
  );
}

export default App;
