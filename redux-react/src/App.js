import { useState } from 'react';
import './App.css';
import { createStore } from 'redux'
import { useDispatch, useSelector } from 'react-redux';


// STORE
const initState = {
  count: 0
}

const reducer = (state = initState, action) => {
  switch (action.type) {
    case 'INC':
      return { ...state, count: state.count + 1 }
    case 'DEC':
      return { ...state, count: state.count - 1 }
    default:
      return state
  }
}

const store = createStore(reducer)
// =========


function App() {
  const dispatch = useDispatch();
  // Subscribe to main store 
  const count = useSelector(store => store.count);

  return (
    <div className="app">
      <div className='counter'>
        <p>{count}</p>
        <button onClick={() => { dispatch({ type: "INC" }) }}>increment</button>
        <button onClick={() => { dispatch({ type: "DEC" }) }}>decrement</button>
      </div>
    </div >
  );
}

export { App, store };
