import React, {useState, useEffect} from 'react';
import ReactDOM from 'react-dom';
import './index.css';

class Counter extends React.Component {
  state = {
    count: 0,
  }

  onValueChange = (value) => {this.setState((oldState) => {
    return {count: oldState.count + value}
  });

  }

  render() {
    return (
      <div className="counter">
        <p>{this.state.count}</p>
        <input type="button" value='Increment' onClick={() => this.onValueChange(1)}/>
        <input type="button" value='Decrement' onClick={() => this.onValueChange(-1)}/>
      </div>
    )
  }
}

const Counter2 = () => {
  const [count, setCount] = useState(0);

  useEffect(() => {
    return () => console.log('>>> WILL UNMOUNT')
  })

  return (
    <div>
      <p>{count}</p>
      <input type="button" value='Increment' onClick={() => {setCount((oldState) => oldState + 1)}}/>
      <input type="button" value='Decrement' onClick={() => {setCount((oldState) => oldState - 1)}}/>
    </div>
  )
}

const NameComponent = () => {
  const [userInfo, setUserInfo] = useState({
    firstName: 'Bakdoolot',
    lastName: 'Altybaev'
  })

  return (
    <div>
      <p>{userInfo.firstName}, {userInfo.lastName}</p>
      <input type="input" onChange={(event) => setUserInfo({...userInfo, firstName: event.target.value})} value={userInfo.firstName} />
      <input type="input" onChange={(event) => setUserInfo({...userInfo, lastName: event.target.value})} value={userInfo.lastName} />
    </div>
  )
}

const App = () => {
  const [showCounter, setShowCounter] = useState(false)

  const counter = showCounter ? <Counter2 /> : null;

  return (
    <div className="app">
    <h1>Hello</h1>
    <Counter/>
    {counter}
    <button onClick={() => setShowCounter(state => !state)}>Toggle Counter</button>
    </div>
  )
}

ReactDOM.render(
    <App />,
  document.getElementById('root')
);
