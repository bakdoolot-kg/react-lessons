export const todoLabel = [
  { id: 1, label: "Create app", done: false, important: false },
  { id: 2, label: "Drink tea", done: false, important: false },
  { id: 3, label: "Learn React", done: false, important: false },
  { id: 4, label: "Relax", done: false, important: false },
  { id: 5, label: "learn react dev tools", done: false, important: false },
];
