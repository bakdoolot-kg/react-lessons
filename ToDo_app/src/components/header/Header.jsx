import React from 'react';
import './header.css'
import {Search, Filter, AddTodo} from "../index";


const Header = () => {
    return (
        <div className='header'>
            <h1>To Do app</h1>
            <AddTodo/>
            <div className="header__inner">
            <Search/>
            <Filter/>
            </div>
        </div>
    );
};

export default Header;