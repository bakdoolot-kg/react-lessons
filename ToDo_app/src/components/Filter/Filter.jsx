import React from 'react';
import './filter.css'

const Filter = () => {
    return (
        <div className={'Filter'}>
            <span className='done'>
                active
            </span>
            <span className='important'>
                important
            </span>
        </div>
    );
};

export default Filter;