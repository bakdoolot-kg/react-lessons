import React from "react";
import "./search.css";

const Search = () => {
  return (
    <div className="Search">
      <input type="text" placeholder={"Search task"} />
    </div>
  );
};

export default Search;
