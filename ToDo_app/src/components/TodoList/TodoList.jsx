import React from "react";
import "./todoList.css";
import { TodoListItem } from "../index";

const TodoList = ({ todos }) => {
  return (
    <div className="todoList">
      <ul className="todoListItem-list">
        {todos.map((item) => {
          return (
            <li className="todoListItem-label" key={item.id}>
              {<TodoListItem {...item} />}
              <p className="todoListItem-time">text time</p>
            </li>
          );
        })}
      </ul>
    </div>
  );
};

export default TodoList;
