import React from 'react';
import './addTodo.css'

const AddTodo = () => {
    return (
        <div className='addTodo'>
            <button className="addTodo-btn">+</button>
        </div>
    );
};

export default AddTodo;