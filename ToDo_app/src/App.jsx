import "./app.css";
import { Header, TodoList } from "./components";
import { todoLabel } from "./dataBase";

function App() {
  return (
    <div className="app">
      <Header />
      <TodoList todos={todoLabel} />
    </div>
  );
}

export default App;
