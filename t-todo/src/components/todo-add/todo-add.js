import React from "react";
import "./todo-add.css";

class TodoAdd extends React.Component {
  state = {
    label: "",
  };

  onValueChange = (text) => {
    this.setState({
      label: text.toLowerCase()
    });
  };

  onAddNewTodo = (event) => {
    event.preventDefault();
    this.props.addNewTodo(this.state.label);
    this.setState({
      label: "",
    });
  };

  render() {

    return (
      <div>
        <form
          onSubmit={this.onAddNewTodo}
          style={{ display: "flex", marginTop: "15px" }}
        >
          <input
            className="form-control"
            onChange={(event) => this.onValueChange(event.target.value)}
            value={this.state.label}
            type="text"
            placeholder="Feel the todo"
          />
          <input className="btn btn-primary" type="submit" value="Add" />
        </form>
      </div>
    );
  }
}

export default TodoAdd;
