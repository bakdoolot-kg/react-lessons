import { DELETE, PATCH, POST } from "./config";

const localCredential = JSON.parse(localStorage.getItem("credentials"));
// const authorization = localStorage === true ? `Bearer ${localCredential.access}` : 'credentials';
const headerInfo = "application/json";

class TodoApi {
  _baseUrl = "http://abdyko.tmweb.ru/api";

  login = (username, password) => {
    fetch(`${this._baseUrl}/token/`, {
      method: "POST",
      headers: { "Content-type": 'application/json' },
      body: JSON.stringify({
        username: username,
        password: password,
      }),
    })
      .then(response => {
        if (response.ok) {
          return response.json();
        } else {
          throw new Error("Something wrong with login");
        }
      })
      .then(data => {
        localStorage.setItem('credentials', JSON.stringify(data));
        window.location.reload();
      })
      .catch(error => {
        localStorage.removeItem("credentials");
        window.location.reload();
      });
  };


  getTodos = () => {
    return fetch(`${this._baseUrl}/v1/todo/`, {
      headers: { "Content-type": headerInfo, "Authorization": `Bearer ${localCredential.access}` },
    }).then(response => { return response.json() })
  };

  createTodo = (label) => {
    return fetch(`${this._baseUrl}/v1/todo/`, {
      method: POST,
      headers: { "Content-type": headerInfo, "Authorization": `Bearer ${localCredential.access}` },
      body: JSON.stringify({ label: label, important: false, done: false }),
    }).then(response => { return response.json() })
  };

  deleteTodo = (id) => {
    return fetch(`${this._baseUrl}/v1/todo/${id}/`, {
      method: DELETE,
      headers: { "Content-type": headerInfo, "Authorization": `Bearer ${localCredential.access}` },
    }).then(response => { return response.text() })
  };

  updateTodo = (id, body) => {
    return fetch(`${this._baseUrl}/v1/todo/${id}/`, {
      method: PATCH,
      headers: { "Content-type": headerInfo, "Authorization": `Bearer ${localCredential.access}` },
      body: JSON.stringify(body),
    }).then(response => { return response.json() })
  };
}

export default TodoApi;
