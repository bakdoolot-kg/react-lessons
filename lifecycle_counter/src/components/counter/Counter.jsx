import React, { Component } from "react";
import "./counter.scss";

class Counter extends Component {
  state = {
    count: 0,
    hasError: false,
    errorInfo: null,
  };

  static getDerivedStateFromError(error) {
    return { errorInfo: error, hasError: true };
  }

  componentDidCatch(error, errorInfo) {
    this.setState({
      error: error,
      errorInfo: errorInfo,
    });
  }

  componentDidMount() {
    // this.timerID = setInterval(() => this.tick(), 1000);
  }

  componentDidUpdate() {}

  componentWillUnmount() {
    this.setState({ count: 0 });
    clearInterval(this.timerID);
  }

  shouldComponentUpdate(nextState) {
    let state = this.state.count !== nextState.count ? true : false;
    return state;
  }

  tick() {
    console.log("Hello world!");
  }

  counterInc = () => {
    this.setState((oldCount) => ({ count: oldCount.count + 1 }));
  };

  counterDec = () => {
    this.setState(({ count }) => ({ count: count - 1 }));
  };
  counterReset = () => {
    this.setState({ count: 0 });
  };

  render() {
    if (this.state.hasError) {
      return <h1>Ошибка {this.state.errorInfo}</h1>;
    }

    console.log(this.state.count);
    return (
      <div className="counter">
        <p className="count">{this.state.count}</p>
        <button className="dec" onClick={this.counterDec}>
          Decrement
        </button>
        <button className="inc" onClick={this.counterInc}>
          Increment
        </button>
        <button className="reset" onClick={this.counterReset}>
          Reset
        </button>
      </div>
    );
  }
}

export default Counter;
