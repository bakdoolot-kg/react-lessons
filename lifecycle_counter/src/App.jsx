import React from "react";
import "./app.css";
import { Header, Counter } from "./components";

class App extends React.Component {
  state = {
    showCounter: true,
    error: null,
    errorInfo: null
  };

  componentDidCatch(error, errorInfo) {
    // Catch errors in any components below and re-render with error message
    this.setState({
      error: error,
      errorInfo: errorInfo
    })
    // You can also log error messages to an error reporting service here
  }

  onToggleCounter = () => {
    this.setState((oldState) => {
      return { showCounter: !oldState.showCounter };
    });
  };

  render() {
    if (this.state.errorInfo) {
      // Error path
      return (
          <div>
            <h2>Something went wrong.</h2>
            <details style={{ whiteSpace: 'pre-wrap' }}>
              {this.state.error && this.state.error.toString()}
              <br />
              {this.state.errorInfo.componentStack}
            </details>
          </div>
      );
    }
    const content = this.state.showCounter ? <Counter /> : null;

    return (
      <div className="app">
        <Header />
        <div>
          {content}
          <button
            className="reset"
            style={{ marginTop: "10px" }}
            onClick={this.onToggleCounter}
          >
            Toggle counter
          </button>
        </div>
      </div>
    );
  }
}

export default App;
